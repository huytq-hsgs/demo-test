/**
 * @author Tran Quang Huy
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.entity;

public class DealingCard extends Dealing {

    private String cardId;

    public DealingCard(int id, String time, String action, int stationId, String cardId) {
        super(id, time, action, stationId);
        this.cardId = cardId;
    }

    public String getInsertQuery(String tableName, int index) {
        return String.format(SQL_INSERT_FORMAT, tableName, index, time, action, cardId, stationId);
    }
}
