package edu.hust.afc;

import edu.hust.afc.boundary.Screen;
import hust.soict.se.customexception.InvalidIDException;

public class Main {

    public static void main(String[] args) {
        try {
            Screen screen = new Screen();
            screen.displayStations();
            screen.displayAllTicketsAndCards();
        } catch (InvalidIDException e) {
            System.out.println("[ERROR] Invalid Input !");
            System.exit(0);
        }
    }
}
