/**
 * Handle 24h ticket's information controlling
 *
 * @author Tran Thi Thu Huong
 * @Version 1.0
 * @since 25/08/2019
 */

package edu.hust.afc.controller;

import edu.hust.afc.db.dao.CertificateDAO;
import edu.hust.afc.db.dao.DealingDayTicketDAO;
import edu.hust.afc.entity.*;
import edu.hust.afc.observer.Subject;
import edu.hust.afc.utils.AFCUtils;
import edu.hust.afc.utils.Constants;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DayTicketController implements CertificateController<DayTicket> {

    private List<DayTicket> dayTickets = Collections.emptyList();
    private CertificateDAO<DayTicket> dayTicketDAO;
    private DealingDayTicketDAO dealingDayTicketDAO;
    private Subject<String> dataDisplay;

    DayTicketController(CertificateDAO<DayTicket> dayTicketDAO,
                        DealingDayTicketDAO dealingDayTicketDAO, Subject<String> dataDisplay) {
        this.dayTicketDAO = dayTicketDAO;
        this.dealingDayTicketDAO = dealingDayTicketDAO;
        this.dataDisplay = dataDisplay;
    }

    @Override
    public List<DayTicket> getCertificatesByIds(List<String> certificateIds) {
        List<String> dayTicketIds = certificateIds.stream().filter(Ticket::isDayTicketId)
                .collect(Collectors.toList());
        dayTickets = dayTicketDAO.getDataByIds(dayTicketIds);
        return dayTickets;
    }

    @Override
    public boolean validate(String barCodeInput, String status, Station station) {
        DayTicket dayTicket = dayTickets.stream()
                .filter(owTicket -> barCodeInput.equals(owTicket.getBarCode())).collect(Collectors.toList()).get(0);
        if (Constants.CHECK_IN.equals(status)) {
            return handleCheckIn(dayTicket, station);
        } else {
            return handleCheckOut(dayTicket, station);
        }
    }

    @Override
    public boolean handleCheckIn(DayTicket dayTicket, Station station) {

        String errorStatus = dayTicket.errorStatusMessage(Constants.CHECK_IN);

        if (errorStatus != null) {
            dataDisplay.notifyChange(dayTicket.toInfo());
            dataDisplay.notifyChange(errorStatus);
            return false;
        }

        if (dayTicket.isNew()) {
            dayTicket.setExpire(AFCUtils.getExpireFromCurrent());
        }
        dayTicket.setStatus(DayTicket.IN_STATION);

        dayTicketDAO.updateData(dayTicket);

        DealingDayTicket dealingDayTicket = new DealingDayTicket(0,
                AFCUtils.getTime(System.currentTimeMillis()),
                Dealing.ACTION_ENTER,
                station.getId(),
                dayTicket.getId());
        dealingDayTicketDAO.save(dealingDayTicket);

        dataDisplay.notifyChange(dayTicket.toInfo());
        return true;
    }

    @Override
    public boolean handleCheckOut(DayTicket dayTicket, Station station) {
        String errorStatus = dayTicket.errorStatusMessage(Constants.CHECK_OUT);

        if (errorStatus != null) {
            dataDisplay.notifyChange(dayTicket.toInfo());
            dataDisplay.notifyChange(errorStatus);
            return false;
        }

        if (dayTicket.isExpired()) {
            dayTicket.setStatus(Ticket.DESTROYED);
        } else {
            dayTicket.setStatus(Ticket.OUT_STATION);
        }

        dayTicketDAO.updateData(dayTicket);

        DealingDayTicket dealingDayTicket = new DealingDayTicket(0,
                AFCUtils.getTime(System.currentTimeMillis()),
                Dealing.ACTION_EXIT,
                station.getId(),
                dayTicket.getId());
        dealingDayTicketDAO.save(dealingDayTicket);

        dataDisplay.notifyChange(dayTicket.toInfo());
        return true;
    }

}
