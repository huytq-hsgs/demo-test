/**
 * @author Tran Quang Huy
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.entity;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class Dealing {
    public static final String ACTION_ENTER = "enter";
    public static final String ACTION_EXIT = "exit";
    protected static final String SQL_INSERT_FORMAT = "INSERT INTO mydb.%s VALUES (%d, '%s', '%s', '%s', %d);";
    protected int id;
    protected String time;
    protected String action;
    protected int stationId;

    @Contract(pure = true)
    protected Dealing(int id, String time, String action, int stationId) {
        this.id = id;
        this.time = time;
        this.action = action;
        this.stationId = stationId;
    }

    protected Dealing(@NotNull ResultSet resultSet) throws SQLException {
        this.id = resultSet.getInt("id");
        this.time = resultSet.getString("time");
        this.action = resultSet.getString("type");
        this.stationId = resultSet.getInt("station_id");
    }

    protected abstract String getInsertQuery(String tableName, int index);
}
