/**
 * @author Tran Thi Thu Huong
 * @version 1
 * @since 01/11/2019
 */

package edu.hust.afc.db.dao;

import edu.hust.afc.db.DBConnection;
import edu.hust.afc.entity.DayTicket;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class DayTicketDAOImpl implements CertificateDAO<DayTicket> {

    private DBConnection dbConnection;

    public DayTicketDAOImpl(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    @Override
    public List<DayTicket> getDataByIds(List<String> ids) {
        return ids.stream().map(this::getDataById).collect(Collectors.toList());
    }

    @Override
    public DayTicket getDataById(String id) {
        String query = "SELECT * FROM day_ticket where id LIKE '" + id + "'";
        DayTicket dayticket = null;

        try {
            Connection connection = dbConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                dayticket = new DayTicket(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dayticket;
    }

    @Override
    public boolean updateData(DayTicket dayTicket) {
        String sql = "UPDATE mydb.day_ticket " +
                "SET day_ticket.status ='" + dayTicket.getStatus() + "', day_ticket.expire = " + dayTicket.getExpire() + " " +
                "WHERE day_ticket.id = '" + dayTicket.getId() + "'";
        try {
            dbConnection.getConnection().prepareStatement(sql).executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
