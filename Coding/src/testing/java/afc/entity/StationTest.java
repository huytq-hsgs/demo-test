package testing.java.afc.entity;

import edu.hust.afc.entity.Station;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StationTest {

    Station fakeStation;

    @Before
    public void setUp() throws Exception {
        fakeStation = (new Station(4, "Chatelet", 11.3f));
    }

    @Test
    public void testToString() {
        String expected = "d. Chatelet";
        Assert.assertEquals(expected, fakeStation.toString());
    }

    @Test
    public void getId() {
        Assert.assertEquals(4, fakeStation.getId());
    }

    @Test
    public void getName() {
        Assert.assertEquals("Chatelet", fakeStation.getName());
    }

    @Test
    public void getDistanceToOrigin() {
        Assert.assertEquals(11.3f, fakeStation.getDistanceToOrigin(), 0.1f);
    }
}