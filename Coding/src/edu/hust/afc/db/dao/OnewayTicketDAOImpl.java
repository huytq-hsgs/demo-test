/**
 * @author Tran Trung Huynh
 * @version 1
 * @since 25/10/2019
 */

package edu.hust.afc.db.dao;

import edu.hust.afc.db.DBConnection;
import edu.hust.afc.entity.OnewayTicket;
import edu.hust.afc.entity.Station;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OnewayTicketDAOImpl implements CertificateDAO<OnewayTicket> {

    private DBConnection dbConnection;

    public OnewayTicketDAOImpl(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    @Override
    public List<OnewayTicket> getDataByIds(List<String> onewayTicketIds) {
        List<OnewayTicket> onewayTickets = new ArrayList<>();

        for (String id : onewayTicketIds) {
            onewayTickets.add(getDataById(id));
        }

        return onewayTickets;
    }

    @Override
    public OnewayTicket getDataById(String id) {
        String sql = "SELECT oneway_ticket.* FROM mydb.oneway_ticket WHERE oneway_ticket.id = '" + id + "'";
        OnewayTicket onewayTicket = null;

        try {
            Connection connection = dbConnection.getConnection();
            PreparedStatement prepareStatement = connection.prepareStatement(sql);
            ResultSet resultSet = prepareStatement.executeQuery();
            if (resultSet.next()) {
                onewayTicket = new OnewayTicket(resultSet);
            }

            int firstStationId = resultSet.getInt("first_station_id");
            int secondStationId = resultSet.getInt("second_station_id");

            if (firstStationId != 0 && secondStationId != 0) {
                sql = "SELECT * FROM mydb.station WHERE station.id =  '" + firstStationId + "' or station.id = '"
                        + secondStationId + "'";
                resultSet = connection.prepareStatement(sql).executeQuery();
                while (resultSet.next()) {
                    if (resultSet.getInt("id") == firstStationId) {
                        onewayTicket.setFirstStation(new Station(resultSet));
                    } else if (resultSet.getInt("id") == secondStationId) {
                        onewayTicket.setSecondStation(new Station(resultSet));
                    }
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return onewayTicket;
    }

    @Override
    public boolean updateData(OnewayTicket onewayTicket) {
        String sql = "UPDATE mydb.oneway_ticket SET oneway_ticket.status ='" + onewayTicket.getStatus()
                + "' WHERE oneway_ticket.id = '" + onewayTicket.getId() + "'";

        try {
            dbConnection.getConnection().prepareStatement(sql).executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
