/**
 * @author Loy Kakda
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.db.dao;

import edu.hust.afc.entity.Station;

import java.util.List;

public interface StationDAO {

    /**
     * Get all stations from database
     *
     * @return list of stations
     */
    List<Station> getStations();

    /**
     * Get a station in database
     *
     * @param id index of station
     * @return a station
     */
    Station getStationById(String id);
}
