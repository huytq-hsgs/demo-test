package testing.java.afc.controller.code;

import edu.hust.afc.db.dao.CodeDAO;
import edu.hust.afc.entity.Code;

import java.util.ArrayList;
import java.util.List;

public class FakeCodeDAO implements CodeDAO {
    private List<Code> codes = new ArrayList<>();

    FakeCodeDAO() {
        codes.add(new Code("bc2dc3d99c8b9b1f", "PC", "PC201905080000"));
        codes.add(new Code("58827b3afb11235e", "TF", "TF201902010001"));
        codes.add(new Code("7213c4464ab8b8af", "OW", "OW201903140003"));
    }

    @Override
    public List<Code> getCodes() {
        return codes;
    }

    @Override
    public Code getCode(String codeValue) {
        switch (codeValue) {
            case "bc2dc3d99c8b9b1f":
                return codes.get(0);
            case "58827b3afb11235e":
                return codes.get(1);
            case "7213c4464ab8b8af":
                return codes.get(2);
        }
        return null;
    }
}
