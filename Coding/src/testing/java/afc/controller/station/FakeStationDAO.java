package testing.java.afc.controller.station;

import edu.hust.afc.db.dao.StationDAO;
import edu.hust.afc.entity.Station;

import java.util.ArrayList;
import java.util.List;

public class FakeStationDAO implements StationDAO {

    private List<Station> stations = new ArrayList<>();

    public FakeStationDAO() {
        stations.add(new Station(1, "Saint-Lazare", 0));
        stations.add(new Station(2, "Madeleine", 5));
        stations.add(new Station(3, "Pyramides", 8.5f));
        stations.add(new Station(4, "Chatelet", 11.3f));
        stations.add(new Station(5, "Gare de Lyon", 15.8f));
        stations.add(new Station(6, "Bercy", 18.9f));
        stations.add(new Station(7, "Cour Saint-Emilion", 22));
        stations.add(new Station(8, "Bibliotheque Francois Mitterrand", 25.3f));
        stations.add(new Station(9, "Olympiades", 28.8f));
    }

    @Override
    public List<Station> getStations() {
        return stations;
    }

    @Override
    public Station getStationById(String id) {
        switch (id) {
            case "a":
                return stations.get(0);
            case "b":
                return stations.get(1);
            case "c":
                return stations.get(2);
            case "d":
                return stations.get(3);
            case "e":
                return stations.get(4);
            case "f":
                return stations.get(5);
            case "g":
                return stations.get(6);
            case "h":
                return stations.get(7);
            case "i":
                return stations.get(8);
        }
        return null;
    }
}
