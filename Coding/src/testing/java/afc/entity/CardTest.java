package testing.java.afc.entity;

import edu.hust.afc.entity.Card;
import edu.hust.afc.entity.Station;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CardTest {

    private Card fakeInStationCard;
    private Card fakeNewCard;
    private Station station;

    @Before
    public void setUp() throws Exception {
        station = new Station(3, "Pyramides", 8.5f);

        fakeNewCard = new Card("PC201905080000", 18.5f);
        fakeInStationCard = new Card("PTOUGMNO", "PC201904120002", 0f, "IN", station);
    }

    @Test
    public void isCardId() {
        Assert.assertTrue(Card.isCardId("PC201905080000"));
        Assert.assertFalse(Card.isCardId("TW201905080000"));
        Assert.assertFalse(Card.isCardId("PC20190508000012"));
    }

    @Test
    public void testToString() {
        String expected = "PTOUGMNO: Prepaid card: 0.0 euros";
        Assert.assertEquals(expected, fakeInStationCard.toString());
    }

    @Test
    public void toInfo() {
        String expected = "Prepaid Card - ID: PC201905080000 - Balance: 18.5 euros";
        Assert.assertEquals(expected, fakeNewCard.toInfo());
    }

    @Test
    public void isInStation() {
        Assert.assertTrue(fakeInStationCard.isInStation());
        Assert.assertFalse(fakeNewCard.isInStation());
    }

    @Test
    public void getId() {
        Assert.assertEquals("PC201905080000", fakeNewCard.getId());
        Assert.assertNotEquals("PC201905080001", fakeNewCard.getId());
    }

    @Test
    public void getBalance() {
        Assert.assertEquals(18.5f, fakeNewCard.getBalance(), 0.1f);
        Assert.assertEquals(0f, fakeInStationCard.getBalance(), 0.1f);
    }

    @Test
    public void getBarCode() {
        Assert.assertEquals("PTOUGMNO", fakeInStationCard.getBarCode());
    }

    @Test
    public void setBalance() {
        Assert.assertNotEquals(5f, fakeNewCard.getBalance());
        fakeNewCard.setBalance(5f);
        Assert.assertEquals(5f, fakeNewCard.getBalance(), 0.1f);
    }

    @Test
    public void getEnterStation() {
        Assert.assertEquals(station, fakeInStationCard.getEnterStation());
        Assert.assertNull(fakeNewCard.getEnterStation());
    }

    @Test
    public void getStatus() {
        Assert.assertEquals(Card.CARD_IN_STATION, fakeInStationCard.getStatus());
    }

    @Test
    public void setStatus() {
        Assert.assertNotEquals(Card.CARD_IN_STATION, fakeNewCard.getStatus());
        fakeNewCard.setStatus(Card.CARD_IN_STATION);
        Assert.assertEquals(Card.CARD_IN_STATION, fakeNewCard.getStatus());
    }

    @Test
    public void setEnterStation() {
        Assert.assertNotEquals(station, fakeNewCard.getEnterStation());
        fakeNewCard.setEnterStation(station);
        Assert.assertEquals(station, fakeNewCard.getEnterStation());
    }
}