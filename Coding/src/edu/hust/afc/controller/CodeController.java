/**
 * Handle code's information controlling
 *
 * @author Tran Trung Huynh
 * @Version 1.0
 * @since 08/11/2019
 */

package edu.hust.afc.controller;

import edu.hust.afc.db.dao.CodeDAO;
import edu.hust.afc.entity.Code;
import edu.hust.afc.utils.AFCUtils;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;
import hust.soict.se.scanner.CardScanner;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CodeController {
    /**
     * Data access object of code
     */
    private CodeDAO codeDAO;

    public CodeController(CodeDAO codeDAO) {
        this.codeDAO = codeDAO;
    }

    /**
     * Get id of card/One-way ticket/24h ticket map with bar codes in external file
     *
     * @return list of id
     * @throws InvalidIDException if wrong input
     */
    public List<String> getTicketsOrCardIds() throws InvalidIDException {
        List<String> barCodes = getBarCodesFromFile();
        List<String> travelingCertificateIds = new ArrayList<>();
        for (String barCode : barCodes) {
            travelingCertificateIds.add(handleBarCode(barCode));
        }

        return codeDAO.getCodes().stream()
                .filter(code -> travelingCertificateIds.contains(code.getCode()))
                .map(Code::getCertificateId)
                .collect(Collectors.toList());
    }

    /**
     * Determine bar code is card/One-way ticket/24h ticket
     *
     * @param barCodeInput bar code
     * @return type of certificate
     * @throws InvalidIDException if wrong input
     */
    public String getCodeType(String barCodeInput) throws InvalidIDException {
        Code code = codeDAO.getCode(handleBarCode(barCodeInput));
        return code.getType();
    }

    /**
     * Get code from bar code via {@link CardScanner} or {@link TicketRecognizer}
     *
     * @param barCode bar code
     * @return code after handle
     * @throws InvalidIDException if wrong input
     */
    private String handleBarCode(String barCode) throws InvalidIDException {
        if (AFCUtils.isCardBarCode(barCode)) {
            return CardScanner.getInstance().process(barCode);
        }
        if (AFCUtils.isTicketBarCode(barCode)) {
            return TicketRecognizer.getInstance().process(barCode);
        }
        throw new InvalidIDException();
    }

    /**
     * Get all list of bar codes from external file
     *
     * @return list of bar codes
     */
    private List<String> getBarCodesFromFile() {
        List<String> barCodes = new ArrayList<>();
        String myCurrentDir = System.getProperty("user.dir");
        try {
            FileInputStream fis = new FileInputStream(myCurrentDir + "\\src\\main\\resources\\bar_codes.txt");
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);

            String line = br.readLine();
            while (line != null) {
                barCodes.add(line);
                line = br.readLine();
            }

            br.close();
            isr.close();
            fis.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return barCodes;
    }
}
