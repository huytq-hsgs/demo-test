/**
 * Handle station's information controlling
 *
 * @author Loy Kakda
 * @Version 1.0
 * @since 08/11/2019
 */

package edu.hust.afc.controller;

import edu.hust.afc.db.dao.StationDAO;
import edu.hust.afc.entity.Station;
import hust.soict.se.customexception.InvalidIDException;

import java.util.Collections;
import java.util.List;

public class StationController {

    /**
     * Data access object of station
     */
    private StationDAO stationDAO;
    /**
     * List of station
     */
    private List<Station> stations = Collections.emptyList();

    public StationController(StationDAO stationDAO) {
        this.stationDAO = stationDAO;
    }

    /**
     * get all stations information
     *
     * @return list of station
     */
    public List<Station> getStations() {
        if (stations.isEmpty()) {
            stations = stationDAO.getStations();
        }
        return stations;
    }

    /**
     * get station information base on its id
     *
     * @param inputId id of station
     * @return Station information
     * @throws InvalidIDException        if wrong input
     * @throws IndexOutOfBoundsException if wrong input
     */
    public Station getStation(char inputId) throws InvalidIDException, IndexOutOfBoundsException {
        int index = inputId - 'a';
        if (index < 0 || index >= stations.size()) {
            throw new InvalidIDException();
        }
        return stations.get(index);
    }

}
