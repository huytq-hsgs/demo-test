/**
 * @author Tran Quang Huy
 * @version 1
 * • @since 25/10/2019
 */

package edu.hust.afc.db.dao;

import edu.hust.afc.db.DBConnection;
import edu.hust.afc.entity.Card;
import edu.hust.afc.entity.Station;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class CardDAOImpl implements CertificateDAO<Card> {

    private DBConnection dbConnection;

    public CardDAOImpl(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    @Override
    public List<Card> getDataByIds(List<String> ids) {
        return ids.stream().map(this::getDataById).collect(Collectors.toList());
    }

    @Override
    public Card getDataById(String id) {
        String query = "SELECT * FROM card where id LIKE '" + id + "'";
        Card card = null;

        try {
            Connection connection = dbConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                card = new Card(resultSet);
            }

            int stationId = resultSet.getInt("enter_station_id");
            if (stationId != 0) {
                query = "SELECT  * FROM station WHERE id = " + stationId;
                resultSet = connection.prepareStatement(query).executeQuery();
                if (resultSet.next() && card != null) {
                    card.setEnterStation(new Station(resultSet));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return card;
    }

    @Override
    public boolean updateData(Card card) {
        int enterStationId = card.getEnterStation() == null ? 0 : card.getEnterStation().getId();
        String query = "UPDATE card " +
                "SET balance = " + card.getBalance() + ", status = '" + card.getStatus() + "', " +
                "enter_station_id = " + enterStationId + ", bar_code = '" + card.getBarCode() + "' " +
                "WHERE id LIKE '" + card.getId() + "'";

        try {
            dbConnection.getConnection().prepareStatement(query).executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
