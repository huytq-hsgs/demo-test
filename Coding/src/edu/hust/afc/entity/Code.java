/**
 * @author Tran Quang Huy
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.entity;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Code {
    public static final String ONE_WAY_TICKET = "OW";
    public static final String DAY_TICKET = "TF";
    public static final String PREPAID_CARD = "PC";
    private String code;
    private String type;
    private String certificateId;

    @Contract(pure = true)
    public Code(String code, String type, String certificateId) {
        this.code = code;
        this.type = type;
        this.certificateId = certificateId;
    }

    public Code(@NotNull ResultSet resultSet) throws SQLException {
        this.code = resultSet.getString(resultSet.findColumn("id"));
        this.type = resultSet.getString(resultSet.findColumn("type"));
        this.certificateId = resultSet.getString(resultSet.findColumn("id_ticket_or_card"));
    }

    public String getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public String getCertificateId() {
        return certificateId;
    }
}
