package testing.java.afc.controller;

import edu.hust.afc.controller.CardController;
import edu.hust.afc.db.dao.CertificateDAO;
import edu.hust.afc.db.dao.DealingCardDAO;
import edu.hust.afc.entity.Card;
import edu.hust.afc.entity.Station;
import edu.hust.afc.observer.Subject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CardControllerTest {

    @InjectMocks
    private CardController cardController;

    @Mock
    private CertificateDAO<Card> cardDAO;

    @Mock
    private Subject<String> dataShowing;

    @Mock
    private DealingCardDAO dealingCardDAO;

    @Mock
    private Subject<Boolean> openingGateStatus;

    private List<String> fakeCertificateIds = new ArrayList<>();
    private Card fakeNewCard;
    private Card fakeInStationCard;
    private Card fakeInStationCard2;
    private Station fakeStation1;
    private Station fakeStation2;
    private List<Card> fakeCards = new ArrayList<>();

    @Before
    public void init() {
        fakeCertificateIds.add("PC201905080000");
        fakeStation1 = new Station(3, "Pyramides", 8.5f);
        fakeStation2 = new Station(4, "Chatelet", 11.3f);

        fakeNewCard = new Card("PC201905080000", 18.5f);
        fakeInStationCard = new Card("PTOUGMNO", "PC201904120002", 0f, "IN", fakeStation1);
        fakeInStationCard2 = new Card("PTOUGMNO", "PC201904120002", 15f, "IN", fakeStation1);

        fakeCards.add(fakeNewCard);

    }

    @Test
    public void getCertificatesByIds_returnListCards() {
        when(cardDAO.getDataByIds(fakeCertificateIds)).thenReturn(fakeCards);

        List<Card> expected = fakeCards;
        List<Card> actual = cardController.getCertificatesByIds(fakeCertificateIds);
        Assert.assertNotNull(actual);
        Assert.assertNotEquals(0, actual.size());
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getCertificatesByIds_returnEmpty() {
        when(cardDAO.getDataByIds(Collections.emptyList())).thenReturn(Collections.emptyList());

        List<Card> actual = cardController.getCertificatesByIds(Collections.emptyList());
        Assert.assertNotNull(actual);
        Assert.assertEquals(0, actual.size());
    }

    @Test
    public void handleCheckOut() {
        Assert.assertFalse(cardController.handleCheckOut(fakeNewCard, fakeStation1));
        Assert.assertFalse(cardController.handleCheckOut(fakeInStationCard, fakeStation2));
        Assert.assertTrue(cardController.handleCheckOut(fakeInStationCard2, fakeStation2));
    }

    @Test
    public void handleCheckIn() {
        Assert.assertFalse(cardController.handleCheckIn(fakeInStationCard, fakeStation1));
        Assert.assertTrue(cardController.handleCheckOut(fakeNewCard, fakeStation2));
    }
}
