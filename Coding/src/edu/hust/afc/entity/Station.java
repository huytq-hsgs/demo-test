/**
 * @author Tran Trung Huynh
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.entity;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Station {
    private int id;
    private String name;
    private float distanceToOrigin;

    @Contract(pure = true)
    public Station(int id, String name, float distanceToOrigin) {
        this.id = id;
        this.name = name;
        this.distanceToOrigin = distanceToOrigin;
    }

    public Station(@NotNull ResultSet resultSet) throws SQLException {
        this.id = resultSet.getInt("id");
        this.name = resultSet.getString("name");
        this.distanceToOrigin = resultSet.getFloat("distanceToOrigin");
    }

    @Override
    public String toString() {
        return (char) (96 + id) + ". " + name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public float getDistanceToOrigin() {
        return distanceToOrigin;
    }
}
