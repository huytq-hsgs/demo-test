/**
 * {@link edu.hust.afc.controller.CardController} control logic, execute data about prepaid card
 * Handle event check in or check out with prepaid card
 *
 * @author Tran Quang Huy
 * @version 1
 * @since 27-10-2019
 */

package edu.hust.afc.controller;

import com.sun.istack.internal.NotNull;
import edu.hust.afc.db.dao.CertificateDAO;
import edu.hust.afc.db.dao.DealingCardDAO;
import edu.hust.afc.entity.Card;
import edu.hust.afc.entity.Dealing;
import edu.hust.afc.entity.DealingCard;
import edu.hust.afc.entity.Station;
import edu.hust.afc.observer.Subject;
import edu.hust.afc.utils.AFCUtils;
import edu.hust.afc.utils.Constants;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CardController implements CertificateController<Card> {

    /**
     * List of card in file
     */
    private List<Card> cards = Collections.emptyList();
    /**
     * receive data to update, and notify to screen to show
     */
    private Subject<String> dataDisplay;
    /**
     * Data access object of card
     */
    private CertificateDAO<Card> cardDAO;
    /**
     * Data access object of dealing card
     */
    private DealingCardDAO dealingCardDAO;

    CardController(CertificateDAO<Card> cardDAO, DealingCardDAO dealingCardDAO, Subject<String> dataDisplay) {
        this.cardDAO = cardDAO;
        this.dealingCardDAO = dealingCardDAO;
        this.dataDisplay = dataDisplay;
    }

    @Override
    public List<Card> getCertificatesByIds(List<String> certificateIds) {
        List<String> cardIds = certificateIds.stream().filter(Card::isCardId).collect(Collectors.toList());
        cards = cardDAO.getDataByIds(cardIds);
        return cards;
    }

    @Override
    public boolean validate(String barCode, String status, @NotNull Station station) {
        Card card = cards.stream().filter(card1 -> barCode.equals(card1.getBarCode())).collect(Collectors.toList()).get(0);
        if (Constants.CHECK_IN.equals(status)) {
            return handleCheckIn(card, station);
        } else {
            return handleCheckOut(card, station);
        }
    }

    @Override
    public boolean handleCheckIn(@NotNull Card card, @NotNull Station station) {

        if (card.isInStation()) {
            dataDisplay.notifyChange(card.toInfo());
            dataDisplay.notifyChange("[ERROR] You need check out with this card first !!");
            return false;
        }
        card.setStatus(Card.CARD_IN_STATION);
        card.setEnterStation(station);
        cardDAO.updateData(card);

        DealingCard dealingCard = new DealingCard(0,
                AFCUtils.getTime(System.currentTimeMillis()),
                Dealing.ACTION_ENTER,
                station.getId(),
                card.getId());
        dealingCardDAO.save(dealingCard);

        dataDisplay.notifyChange(card.toInfo());
        return true;
    }

    @Override
    public boolean handleCheckOut(@NotNull Card card, @NotNull Station station) {

        if (!card.isInStation()) {
            dataDisplay.notifyChange(card.toInfo());
            dataDisplay.notifyChange("[ERROR] You must to check in with this card first !!");
            return false;
        }

        float distance = Math.abs(station.getDistanceToOrigin() - card.getEnterStation().getDistanceToOrigin());
        float money = AFCUtils.calculateTotalFare(distance);
        if (money > card.getBalance()) {
            dataDisplay.notifyChange(card.toInfo());
            dataDisplay.notifyChange("[ERROR] Not enough balance: Expected " + money + " euros ");
            return false;
        }

        card.setStatus(Card.CARD_OUT_STATION);
        card.setEnterStation(null);
        card.setBalance(card.getBalance() - money);
        cardDAO.updateData(card);

        DealingCard dealingCard = new DealingCard(0,
                AFCUtils.getTime(System.currentTimeMillis()),
                Dealing.ACTION_EXIT,
                station.getId(),
                card.getId());
        dealingCardDAO.save(dealingCard);

        dataDisplay.notifyChange(card.toInfo());
        return true;
    }
}
