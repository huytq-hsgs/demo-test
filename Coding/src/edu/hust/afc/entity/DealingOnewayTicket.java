/**
 * @author Tran Trung Huynh
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.entity;

public class DealingOnewayTicket extends Dealing {

    private String onewayTicketId;

    public DealingOnewayTicket(int id, String time, String action, int stationId, String onewayTicketId) {
        super(id, time, action, stationId);
        this.onewayTicketId = onewayTicketId;
    }

    @Override
    public String getInsertQuery(String tableName, int index) {
        return String.format(SQL_INSERT_FORMAT, tableName, index, time, action, onewayTicketId, stationId);
    }
}
