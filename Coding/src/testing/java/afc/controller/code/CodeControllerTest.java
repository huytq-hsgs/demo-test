package testing.java.afc.controller.code;

import edu.hust.afc.controller.CodeController;
import edu.hust.afc.db.dao.CodeDAO;
import hust.soict.se.customexception.InvalidIDException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CodeControllerTest {

    private CodeController codeController;

    private CodeDAO codeDAO;

    private List<String> barCodes = new ArrayList<>();

    @Before
    public void init() {
        codeDAO = new FakeCodeDAO();
        codeController = new CodeController(codeDAO);

        barCodes.add("fkdodirn");
        barCodes.add("poiuytre");
        barCodes.add("kdifornd");
        barCodes.add("lkjhgfds");
        barCodes.add("KFOAIDMF");
    }

    @Test
    public void getCodeType_returnCard() {
        String expected = "PC", actual;

        try {
            actual = codeController.getCodeType("KFOAIDMF");
        } catch (Exception exception) {
            actual = null;
        }
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getCodeType_return24h() {
        try {
            String expected = "TF";
            String actual = codeController.getCodeType("oepwlguc");
            Assert.assertEquals(expected, actual);
        } catch (Exception exception) {
            Assert.fail();
        }

    }

    @Test
    public void getCodeType_returnOneway() {
        try {
            String expected = "OW";
            String actual = codeController.getCodeType("poiuytre");
            Assert.assertEquals(expected, actual);
        } catch (Exception exception) {
            Assert.fail();
        }
    }

    @Test
    public void getCodeType_catchError() {
        String actual;
        try {
            actual = codeController.getCodeType("ueyddfsr");
        } catch (InvalidIDException | NullPointerException e) {
            actual = null;
        }
        Assert.assertNull(actual);
    }

    @Test
    public void getTicketsOrCardIds_returnIds() {
        try {
            List<String> actual = codeController.getTicketsOrCardIds();
            Assert.assertEquals(2, actual.size());
            Assert.assertEquals("PC201905080000", actual.get(0));
            Assert.assertEquals("OW201903140003", actual.get(1));

        } catch (Exception e) {
            Assert.fail();
        }
    }
}
