/**
 * @author Tran Thi Thu Huong
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.entity;

public class DealingDayTicket extends Dealing {
    private String dayTicketId;

    public DealingDayTicket(int id, String time, String action, int stationId, String ticketId) {
        super(id, time, action, stationId);
        this.dayTicketId = ticketId;
    }

    @Override
    public String getInsertQuery(String tableName, int index) {
        return String.format(SQL_INSERT_FORMAT, tableName, index, time, action, dayTicketId, stationId);
    }
}
