package testing.java.afc.controller.station;

import edu.hust.afc.controller.StationController;
import edu.hust.afc.db.dao.StationDAO;
import edu.hust.afc.entity.Station;
import hust.soict.se.customexception.InvalidIDException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StationControllerTest {

    @InjectMocks
    private StationController stationController;

    @Mock
    private StationDAO stationDAO;

    @Mock
    private List<Station> stations;

    private List<Station> fakeStations = new ArrayList<>();
    private Station fakeStation;

    @Before
    public void setUp() {
        fakeStation = new Station(1, "Saint-Lazare", 0);
        fakeStations.add(fakeStation);
    }

    @Test
    public void getStations_returnStations() {
        when(stationDAO.getStations()).thenReturn(fakeStations);

        List<Station> actual = stationController.getStations();
        Assert.assertNotNull(actual);
        Assert.assertNotEquals(0, actual.size());
        Assert.assertEquals(fakeStations, actual);
    }

    @Test
    public void getStation_catchError() {
        Station actual;
        try {
            actual = stationController.getStation('1');
        } catch (InvalidIDException | IndexOutOfBoundsException e) {
            actual = null;
        }
        Assert.assertNull(actual);
    }
}
