/**
 * @author Loy Kakda
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.db.dao;

import edu.hust.afc.db.DBConnection;
import edu.hust.afc.entity.Code;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CodeDAOImpl implements CodeDAO {

    private DBConnection dbConnection;

    public CodeDAOImpl(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    @Override
    public List<Code> getCodes() {
        String query = "SELECT * FROM ticket_card_all";

        List<Code> codes = new ArrayList<>();

        try {
            Connection connection = dbConnection.getConnection();
            if (connection == null) return codes;
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                codes.add(new Code(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return codes;
    }

    @Override
    public Code getCode(String codeValue) {
        String query = "SELECT * FROM ticket_card_all WHERE ticket_card_all.id LIKE '" + codeValue + "'";

        try {
            Connection connection = dbConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return new Code(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
