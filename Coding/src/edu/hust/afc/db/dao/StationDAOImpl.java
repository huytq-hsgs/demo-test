/**
 * @author Loy Kakda
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.db.dao;

import edu.hust.afc.db.DBConnection;
import edu.hust.afc.entity.Station;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StationDAOImpl implements StationDAO {

    private DBConnection dbConnection;

    public StationDAOImpl(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    @Override
    public List<Station> getStations() {
        String query = "SELECT * FROM station";

        List<Station> stations = new ArrayList<>();

        try {
            Connection connection = dbConnection.getConnection();
            if (connection == null) return stations;
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                stations.add(new Station(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return stations;
    }

    @Override
    public Station getStationById(String id) {
        String query = "SELECT * FROM station WHERE id LIKE " + id;

        Station station = null;

        try {
            Connection connection = dbConnection.getConnection();
            if (connection == null) return null;
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) station = new Station(resultSet);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return station;
    }
}
