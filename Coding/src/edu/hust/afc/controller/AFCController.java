/**
 * Control logic, execute data about prepaid card
 * Handle event check in or check out with prepaid card
 *
 * @author Tran Quang Huy
 * @version 1
 * @since 08-11-2019
 */

package edu.hust.afc.controller;

import edu.hust.afc.db.DBConnection;
import edu.hust.afc.db.dao.*;
import edu.hust.afc.entity.*;
import edu.hust.afc.observer.Subject;
import edu.hust.afc.utils.AFCUtils;
import edu.hust.afc.utils.Constants;
import hust.soict.se.customexception.InvalidIDException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AFCController {
    /**
     * Data to display to screen
     */
    private Subject<String> dataDisplay = new Subject<>();
    /**
     * Control logic and data about code
     */
    private CodeController codeController;
    /**
     * Control logic and data about card
     */
    private CertificateController<Card> cardController;
    /**
     * Control logic and data about one way ticket
     */
    private CertificateController<OnewayTicket> onewayTicketController;
    /**
     * Control logic and data about 24h ticket
     */
    private CertificateController<DayTicket> dayTicketController;

    /**
     * Control logic and data about station
     */
    private StationController stationController;

    /**
     * Action of user (Check in/Check out)
     */
    private String action = Constants.CHECK_IN;
    /**
     * Station when user check in or check out
     */
    private Station station = null;

    public AFCController() {
        initComponents();
    }

    /**
     * init components : dao, controller, database connection, ...
     */
    private void initComponents() {
        DBConnection dbConnection = DBConnection.getInstance();

        StationDAO stationDAO = new StationDAOImpl(dbConnection);
        CodeDAO codeDAO = new CodeDAOImpl(dbConnection);
        CertificateDAO<Card> cardDAO = new CardDAOImpl(dbConnection);
        CertificateDAO<OnewayTicket> onewayTicketDAO = new OnewayTicketDAOImpl(dbConnection);
        CertificateDAO<DayTicket> dayTicketDAO = new DayTicketDAOImpl(dbConnection);

        DealingCardDAO dealingCardDAO = new DealingCardDAOImpl(dbConnection);
        DealingOnewayTicketDAO dealingOnewayTicketDAO = new DealingOnewayTicketDAOImpl(dbConnection);
        DealingDayTicketDAO dealingDayTicketDAO = new DealingDayTicketDAOImpl(dbConnection);

        stationController = new StationController(stationDAO);
        codeController = new CodeController(codeDAO);
        cardController = new CardController(cardDAO, dealingCardDAO, dataDisplay);
        onewayTicketController = new OnewayTicketController(onewayTicketDAO, dealingOnewayTicketDAO, dataDisplay);
        dayTicketController = new DayTicketController(dayTicketDAO, dealingDayTicketDAO, dataDisplay);
    }

    /**
     * Get all station's information
     *
     * @return list of stations
     */
    public List<Station> getStations() {
        return stationController.getStations();
    }

    /**
     * handle input about station
     *
     * @param input :  input of user
     * @throws InvalidIDException if wrong input
     */
    public void handleStationInput(String input) throws InvalidIDException {
        try {
            action = input.split("-")[0];
            char stationId = input.split("-")[1].charAt(0);
            station = stationController.getStation(stationId);
        } catch (Exception e) {
            throw new InvalidIDException();
        }
    }

    /**
     * Get tickets or cards
     *
     * @return list of tickets/cards information
     * @throws InvalidIDException if wrong input
     */
    public List<String> getTicketOrCards() throws InvalidIDException {

        List<String> certificateIds = codeController.getTicketsOrCardIds();

        List<Card> cards = cardController.getCertificatesByIds(certificateIds);
        List<OnewayTicket> onewayTickets = onewayTicketController.getCertificatesByIds(certificateIds);
        List<DayTicket> dayTickets = dayTicketController.getCertificatesByIds(certificateIds);

        List<String> certificates = new ArrayList<>();
        certificates.addAll(onewayTickets.stream().map(OnewayTicket::toString).collect(Collectors.toList()));
        certificates.addAll(dayTickets.stream().map(DayTicket::toString).collect(Collectors.toList()));
        certificates.addAll(cards.stream().map(Card::toString).collect(Collectors.toList()));

        return certificates;
    }

    /**
     * Handle bar code input
     *
     * @param inputBarCode bar code input
     * @throws InvalidIDException if wrong input
     */
    public void handleBarCodeInput(String inputBarCode) throws InvalidIDException {
        String codeType;
        try {
            codeType = codeController.getCodeType(inputBarCode);
        } catch (Exception exception) {
            throw new InvalidIDException();
        }

        boolean isValid = false;
        switch (codeType) {
            case Code.PREPAID_CARD:
                isValid = cardController.validate(inputBarCode, action, station);
                break;
            case Code.ONE_WAY_TICKET:
                isValid = onewayTicketController.validate(inputBarCode, action, station);
                break;
            case Code.DAY_TICKET:
                isValid = dayTicketController.validate(inputBarCode, action, station);
                break;
        }

        if (isValid) {
            AFCUtils.executeGateAutomatically();
        }
    }

    /**
     * Get subject which observe about result can print to screen
     *
     * @return subject of string
     */
    public Subject<String> getDataDisplay() {
        return dataDisplay;
    }
}
