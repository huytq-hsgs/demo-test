/**
 * @author Tran Trung Huynh
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.db.dao;

import edu.hust.afc.db.DBConnection;
import edu.hust.afc.entity.DealingOnewayTicket;
import edu.hust.afc.entity.Station;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DealingOnewayTicketDAOImpl implements DealingOnewayTicketDAO {

    private DBConnection dbConnection;

    public DealingOnewayTicketDAOImpl(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    @Override
    public boolean save(DealingOnewayTicket dealingOnewayTicket) {
        String sql = "SELECT id FROM mydb.dealing_oneway_ticket ORDER BY id DESC LIMIT 1";
        try {
            Connection connection = dbConnection.getConnection();
            ResultSet resultSet = connection.prepareStatement(sql).executeQuery();
            int index = resultSet.next() ? resultSet.getInt("id") + 1 : 0;
            connection.prepareStatement(dealingOnewayTicket.getInsertQuery("dealing_oneway_ticket", index))
                    .executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Station getStationByOnewayTicketId(String onewayTicketId) {
        String sql = "SELECT station.* FROM mydb.dealing_oneway_ticket, mydb.station WHERE dealing_oneway_ticket.oneway_ticket_id = '"
                + onewayTicketId + "' and station.id = dealing_oneway_ticket.station_id";

        try {
            Connection connection = dbConnection.getConnection();
            ResultSet resultSet = connection.prepareStatement(sql).executeQuery();
            if (resultSet.next()) {
                return new Station(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
