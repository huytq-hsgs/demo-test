/**
 * @author Tran Quang Huy
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.db.dao;

import edu.hust.afc.entity.DealingDayTicket;

public interface DealingDayTicketDAO {

    boolean save(DealingDayTicket dealing);
}
