/**
 * @author Tran Quang Huy
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

    private static final String DATABASE_NAME = "mydb";
    private static final String HOST = "localhost";
    private static final String PORT = "3306";
    private static final String USER_NAME = "root";
    private static final String PASSWORD = "123456";
    private static final String URL_FORMAT = "jdbc:mysql://%s:%s/%s?autoReconnect=true&useSSL=false&characterEncoding=utf-8";

    /**
     * Instance of DBConnection
     */
    private static DBConnection INSTANCE;
    /**
     * Connection is saved when connect with database
     */
    private Connection connection;

    private DBConnection() {
    }

    public static DBConnection getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DBConnection();
        }
        return INSTANCE;
    }

    /**
     * Create a connection with MySQL by driver
     *
     * @return a connection to the database
     * @throws SQLException the connecting with database may cause some error or fail concern with SQL
     */
    public Connection getConnection() throws SQLException {
        if (connection == null) {
            String url = String.format(URL_FORMAT, HOST, PORT, DATABASE_NAME);
            connection = DriverManager.getConnection(url, USER_NAME, PASSWORD);
        }
        return connection;
    }
}