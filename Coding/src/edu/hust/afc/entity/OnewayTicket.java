/**
 * @author Tran Trung Huynh
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.entity;

import edu.hust.afc.utils.Constants;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OnewayTicket extends Ticket {
    private float balance;
    private Station firstStation;
    private Station secondStation;

    public OnewayTicket(String id, String status, float balance, Station firstStation, Station secondStation,
                        String barCode) {
        super(id, status, barCode);
        this.balance = balance;
        this.firstStation = firstStation;
        this.secondStation = secondStation;
    }

    public OnewayTicket(ResultSet resultSet) throws SQLException {
        super(resultSet);
        this.balance = resultSet.getFloat("balance");
        this.firstStation = null;
        this.secondStation = null;
    }

    public Station getFirstStation() {
        return this.firstStation;
    }

    public void setFirstStation(Station station) {
        this.firstStation = station;
    }

    public Station getSecondStation() {
        return this.secondStation;
    }

    public void setSecondStation(Station station) {
        this.secondStation = station;
    }

    public float getBalance() {
        return this.balance;
    }

    @Override
    public String toInfo() {
        return "\nType: One-way ticket\nID: " + this.id + "\nBalance: " + this.balance + " euros\n";
    }

    @Override
    public String toString() {
        return barCode + ": One-way ticket between " + firstStation.getName() + " and " + secondStation.getName() + ": "
                + status + " - " + balance + " euros";
    }

    @Override
    public String errorStatusMessage(String action) {
        if (isDestroyed()) {
            return "Your ticket is destroyed !";
        }

        if (Constants.CHECK_IN.equals(action) && isInStation()) {
            return "Your ticket is in use !";
        }

        if (Constants.CHECK_OUT.equals(action) && isNew()) {
            return "Your ticket is new !";
        }

        return null;
    }

    /**
     * Return an error if has error and null if has not error
     *
     * @param currentStation - station where exit or enter
     * @return String
     */
    public String errorPlatformAreaMessage(Station currentStation) {
        boolean isValidEnterPlatformArea = (currentStation.getDistanceToOrigin() - firstStation.getDistanceToOrigin())
                * (currentStation.getDistanceToOrigin() - secondStation.getDistanceToOrigin()) <= 0;
        if (!isValidEnterPlatformArea) {
            return "You can not enter this station because this station don't belong to platform area !";
        }
        return null;
    }
}
