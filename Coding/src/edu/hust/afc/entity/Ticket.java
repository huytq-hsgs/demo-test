/**
 * @author Tran Trung Huynh
 * @version 1
 * @since 25/10/2019
 */

package edu.hust.afc.entity;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.sql.ResultSet;
import java.sql.SQLException;

import static edu.hust.afc.entity.Code.DAY_TICKET;
import static edu.hust.afc.entity.Code.ONE_WAY_TICKET;

public abstract class Ticket {
    public static final String NEW = "New";
    public static final String IN_STATION = "In station";
    public static final String OUT_STATION = "Out station";
    public static final String DESTROYED = "Destroyed";
    protected String id;
    protected String status;
    protected String barCode;

    @Contract(pure = true)
    public Ticket(String id, String status, String barCode) {
        this.id = id;
        this.status = status;
        this.barCode = barCode;
    }

    public Ticket(@NotNull ResultSet resultSet) throws SQLException {
        this.id = resultSet.getString("id");
        this.status = resultSet.getString("status");
        this.barCode = resultSet.getString("bar_code");
    }

    /**
     * Check ticket is one-way ticket or not
     *
     * @param ticketId - id of ticket
     * @return boolean
     */
    public static boolean isOneWayTicketId(@NotNull String ticketId) {
        return ONE_WAY_TICKET.equals(ticketId.substring(0, 2));
    }

    public static boolean isDayTicketId(@NotNull String ticketId) {
        return DAY_TICKET.equals(ticketId.substring(0, 2));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    /**
     * Format info of ticket for displaying
     *
     * @return String
     */
    public abstract String toInfo();

    /**
     * Return an error if has error and null if has not error
     *
     * @param action - string of action exit/enter
     * @return String
     */
    public abstract String errorStatusMessage(String action);

    public boolean isNew() {
        return NEW.equals(status);
    }

    public boolean isDestroyed() {
        return DESTROYED.equals(status);
    }

    public boolean isInStation() {
        return IN_STATION.equals(status);
    }
}
