package testing.java.afc.controller;

import edu.hust.afc.controller.AFCController;
import edu.hust.afc.controller.CertificateController;
import edu.hust.afc.controller.CodeController;
import edu.hust.afc.controller.StationController;
import edu.hust.afc.entity.Card;
import edu.hust.afc.entity.DayTicket;
import edu.hust.afc.entity.OnewayTicket;
import edu.hust.afc.entity.Station;
import edu.hust.afc.observer.Subject;
import edu.hust.afc.utils.Constants;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class AFCControllerTest {

    @InjectMocks
    private AFCController afcController;

    @Mock
    private Subject<Boolean> gateStatus;
    @Mock
    private Subject<String> result;
    @Mock
    private CodeController codeController;
    @Mock
    private CertificateController<Card> cardController;
    @Mock
    private CertificateController<OnewayTicket> onewayTicketController;
    @Mock
    private CertificateController<DayTicket> dayTicketController;
    @Mock
    private StationController stationController;
    @Mock
    private String action = Constants.CHECK_IN;
    @Mock
    private Station station = null;


}
