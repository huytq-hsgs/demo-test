/**
 * Handle one-way ticket information controlling
 *
 * @author Tran Trung Huynh
 * @Version 1.0
 * @since 25/08/2019
 */

package edu.hust.afc.controller;

import edu.hust.afc.db.dao.CertificateDAO;
import edu.hust.afc.db.dao.DealingOnewayTicketDAO;
import edu.hust.afc.entity.*;
import edu.hust.afc.observer.Subject;
import edu.hust.afc.utils.AFCUtils;
import edu.hust.afc.utils.Constants;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class OnewayTicketController implements CertificateController<OnewayTicket> {

    private List<OnewayTicket> onewayTickets = Collections.emptyList();

    private CertificateDAO<OnewayTicket> onewayTicketDAO;
    private DealingOnewayTicketDAO dealingOnewayTicketDAO;
    private Subject<String> dataDisplay;

    OnewayTicketController(CertificateDAO<OnewayTicket> onewayTicketDAO,
                           DealingOnewayTicketDAO dealingOnewayTicketDAO, Subject<String> dataDisplay) {
        this.onewayTicketDAO = onewayTicketDAO;
        this.dealingOnewayTicketDAO = dealingOnewayTicketDAO;
        this.dataDisplay = dataDisplay;
    }

    @Override
    public List<OnewayTicket> getCertificatesByIds(List<String> certificateIds) {
        List<String> onewayTicketIds = certificateIds.stream().filter(Ticket::isOneWayTicketId)
                .collect(Collectors.toList());
        onewayTickets = onewayTicketDAO.getDataByIds(onewayTicketIds);
        return onewayTickets;
    }

    @Override
    public boolean validate(String barCodeInput, String status, Station station) {
        OnewayTicket onewayTicket = onewayTickets.stream()
                .filter(owTicket -> barCodeInput.equals(owTicket.getBarCode())).collect(Collectors.toList()).get(0);
        if (Constants.CHECK_IN.equals(status)) {
            return handleCheckIn(onewayTicket, station);
        } else {
            return handleCheckOut(onewayTicket, station);
        }
    }

    @Override
    public boolean handleCheckIn(@NotNull OnewayTicket onewayTicket, @NotNull Station station) {

        String errorStatus = onewayTicket.errorStatusMessage(Constants.CHECK_IN);
        String errorPlatformArea = onewayTicket.errorPlatformAreaMessage(station);

        if (errorStatus != null) {
            dataDisplay.notifyChange(onewayTicket.toInfo());
            dataDisplay.notifyChange(errorStatus);
            return false;
        } else if (errorPlatformArea != null) {
            dataDisplay.notifyChange(onewayTicket.toInfo());
            dataDisplay.notifyChange(errorPlatformArea);
            return false;
        }

        onewayTicket.setStatus(OnewayTicket.IN_STATION);

        onewayTicketDAO.updateData(onewayTicket);

        DealingOnewayTicket dealingOnewayTicket = new DealingOnewayTicket(0,
                AFCUtils.getTime(System.currentTimeMillis()),
                Dealing.ACTION_ENTER,
                station.getId(),
                onewayTicket.getId());
        dealingOnewayTicketDAO.save(dealingOnewayTicket);

        dataDisplay.notifyChange(onewayTicket.toInfo());
        return true;
    }

    @Override
    public boolean handleCheckOut(OnewayTicket onewayTicket, Station station) {

        String errorStatus = onewayTicket.errorStatusMessage(Constants.CHECK_OUT);

        if (errorStatus != null) {
            dataDisplay.notifyChange(onewayTicket.toInfo());
            dataDisplay.notifyChange(errorStatus);
            return false;
        }

        Station enteredStation = dealingOnewayTicketDAO.getStationByOnewayTicketId(onewayTicket.getId());
        float payment = AFCUtils.calculateTotalFare(Math.abs(station.getDistanceToOrigin() - enteredStation.getDistanceToOrigin()));

        if (payment > onewayTicket.getBalance()) {
            dataDisplay.notifyChange(onewayTicket.toInfo());
            dataDisplay.notifyChange("[ERROR] Not enough balance: Expected " + payment + " euros !");
            return false;
        }

        onewayTicket.setStatus(OnewayTicket.DESTROYED);

        onewayTicketDAO.updateData(onewayTicket);

        DealingOnewayTicket dealingOnewayTicket = new DealingOnewayTicket(0,
                AFCUtils.getTime(System.currentTimeMillis()),
                Dealing.ACTION_EXIT,
                station.getId(),
                onewayTicket.getId());
        dealingOnewayTicketDAO.save(dealingOnewayTicket);

        dataDisplay.notifyChange(onewayTicket.toInfo());
        return true;
    }

}
