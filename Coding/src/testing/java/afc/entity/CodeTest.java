package testing.java.afc.entity;

import edu.hust.afc.entity.Code;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CodeTest {

    private Code code;

    @Before
    public void setUp() throws Exception {
        code = new Code("bc2dc3d99c8b9b1f", "PC", "PC201905080000");
    }

    @Test
    public void getCode() {
        Assert.assertEquals("bc2dc3d99c8b9b1f", code.getCode());
    }

    @Test
    public void getType() {
        Assert.assertEquals(Code.PREPAID_CARD, code.getType());
    }

    @Test
    public void getCertificateId() {
        Assert.assertEquals("PC201905080000", code.getCertificateId());
    }
}