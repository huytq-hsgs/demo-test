/**
 * @author Tran Thi Thu Huong
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.db.dao;

import edu.hust.afc.db.DBConnection;
import edu.hust.afc.entity.DealingDayTicket;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DealingDayTicketDAOImpl implements DealingDayTicketDAO {

    private DBConnection dbConnection;

    public DealingDayTicketDAOImpl(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    @Override
    public boolean save(DealingDayTicket dealingDayTicket) {
        String sql = "SELECT id FROM mydb.dealing_day_ticket ORDER BY id DESC LIMIT 1";
        try {
            Connection connection = dbConnection.getConnection();
            ResultSet resultSet = connection.prepareStatement(sql).executeQuery();
            int index = resultSet.next() ? resultSet.getInt("id") + 1 : 0;
            connection.prepareStatement(dealingDayTicket.getInsertQuery("dealing_day_ticket", index))
                    .executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
