/**
 * @author Tran Thi Thu Huong
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.entity;

import edu.hust.afc.utils.AFCUtils;
import edu.hust.afc.utils.Constants;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DayTicket extends Ticket {

    private static final long ONE_DAY_MILLIS = 86400000L;
    private long expire;

    public DayTicket(ResultSet resultSet) throws SQLException {
        super(resultSet);
        this.expire = resultSet.getLong("expire");
    }

    @Override
    public String toInfo() {
        return "\nType: 24h ticket - ID: " + id + " - Valid until : " + AFCUtils.getTime(expire) + "\n";
    }


    public long getExpire() {
        return this.expire;
    }

    public void setExpire(long expire) {
        this.expire = expire;
    }


    @Override
    public String toString() {
        if (isNew() || isDestroyed()) {
            return barCode + ": 24h tickets: " + status;
        }
        return barCode + ": 24h tickets: Valid until " + AFCUtils.getTime(expire);
    }

    @Override
    public boolean isNew() {
        return super.isNew() || expire == 0;
    }

    @Override
    public String errorStatusMessage(String action) {
        StringBuilder builder = new StringBuilder("Invalid 24h Ticket\n");

        if (isDestroyed()) {
            return builder.append("Your ticket was destroyed !").toString();
        }

        switch (action) {
            case Constants.CHECK_IN:
                if (isExpired()) {
                    return builder.append("Expired: Try to enter at ")
                            .append(AFCUtils.getTime(System.currentTimeMillis()))
                            .toString();
                }
                if (isInStation() || isDestroyed()) {
                    return builder.append("Ticket is in station or destroy, You must check out first!").toString();
                }
                break;
            case Constants.CHECK_OUT:
                if (!isInStation()) {
                    return builder.append("Ticket is new, You must check in first!").toString();
                }
                break;
        }
        return null;
    }

    public boolean isExpired() {
        long current = System.currentTimeMillis();
        return expire != 0 && (expire < current || current < expire - ONE_DAY_MILLIS);
    }
}
