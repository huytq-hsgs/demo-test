/**
 * interface for card/ticket controller
 *
 * @author Tran Quang Huy
 * @Version 1.0
 * @since 08/11/2019
 */

package edu.hust.afc.controller;

import com.sun.istack.internal.NotNull;
import edu.hust.afc.entity.Station;

import java.util.List;

public interface CertificateController<T> {

    /**
     * Get all ready certificate(type: {@link T}) from list of bar code
     *
     * @param certificateIds  list of ready bar code
     *                       we create a list of {@code cardIds} from {@code certificateIds} by use a filter
     * @return list of prepaid cards/tickets
     */
    List<T> getCertificatesByIds(List<String> certificateIds);

    /**
     * Validate when user scan the prepaid card or ticket
     *
     * @param barCode  The bar code of prepaid card or ticket from input
     * @param status   Check in/Check out
     * @param station  Current station where card/ticket is scanned
     * @return validating is successful or failed
     */
    boolean validate(@NotNull String barCode, @NotNull String status, @NotNull Station station);

    /**
     * Handle card/ticket checking in
     *
     * @param certificate  Information of current card/ticket
     * @param station      Current station where card is scanned
     * @return checking is successful or failed
     */
    boolean handleCheckIn(@NotNull T certificate, @NotNull Station station);

    /**
     * Handle card/ticket checking out
     *
     * @param certificate  Information of current card/ticket
     * @param station      Current station where card is scanned
     * @return checking is successful or failed
     */
    boolean handleCheckOut(@NotNull T certificate, @NotNull Station station);
}
