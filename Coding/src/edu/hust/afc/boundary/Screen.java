/**
 * @author Tran Quang Huy
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.boundary;

import edu.hust.afc.controller.AFCController;
import edu.hust.afc.entity.Station;
import hust.soict.se.customexception.InvalidIDException;

import java.util.List;
import java.util.Scanner;

public class Screen {
    private Scanner scanner = new Scanner(System.in);
    private AFCController afcController;

    public Screen() {
        afcController = new AFCController();
        observerData();
    }

    /**
     * Observe data from subjects in controller
     */
    private void observerData() {
        afcController.getDataDisplay().attach(System.out::println);
    }

    /**
     * Display list of station information
     *
     * @throws InvalidIDException if wrong input
     */
    public void displayStations() throws InvalidIDException {
        List<Station> stations = afcController.getStations();

        System.out.println("------------------------------------------------------------------------------");
        System.out.println("There are stations in the line M14 of Paris: \n");
        stations.forEach(System.out::println);
        System.out.println("\nAvailable actions: 1-enter station, 2-exit station");
        System.out.println(
                "\nYou can provide a combination of number (1 or 2) and a letter from (a to i) to enter or exit a station (using hyphen in between). For instance, the combination \"2-d\" will bring you to exit the station Chatelet.");

        System.out.print("\nInput station you want to enter/exit: ");
        String inputStatusAndStation = scanner.nextLine();
        System.out.println("\nYour input: " + inputStatusAndStation);

        afcController.handleStationInput(inputStatusAndStation);
    }

    /**
     * Display tickets and cards
     *
     * @throws InvalidIDException if wrong input
     */
    public void displayAllTicketsAndCards() throws InvalidIDException {

        System.out.println("\n-------------------------------------------------------------------------------");
        System.out.println("These are existing tickets/card:\n");
        showData(afcController.getTicketOrCards());
        System.out.print("\nPlease provide the ticket/card code you want to enter/exit: ");

        String barCodeInput = scanner.nextLine();
        System.out.println("\nYour input: " + barCodeInput);

        afcController.handleBarCodeInput(barCodeInput);
    }

    /**
     * show messages return to screen
     */
    private void showData(List<String> messages) {
        System.out.println("---------------------------------------------------------------------------------");
        messages.forEach(System.out::println);
    }
}
