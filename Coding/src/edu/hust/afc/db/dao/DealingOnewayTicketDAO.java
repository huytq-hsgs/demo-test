/**
 * @author Tran Trung Huynh
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.db.dao;

import edu.hust.afc.entity.DealingOnewayTicket;
import edu.hust.afc.entity.Station;

public interface DealingOnewayTicketDAO {

    /**
     * Store dealing of one-way ticket
     *
     * @param dealing - instance of DealingOnewayTicket class
     * @return boolean
     */
    boolean save(DealingOnewayTicket dealing);

    /**
     * Get station base on ticket's id in dealing history
     *
     * @param onewayTicketId - string id of one-way ticket
     * @return Station
     */
    Station getStationByOnewayTicketId(String onewayTicketId);
}
