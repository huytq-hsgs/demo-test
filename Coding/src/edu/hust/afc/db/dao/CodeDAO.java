/**
 * @author Loy Kakda
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.db.dao;

import edu.hust.afc.entity.Code;

import java.util.List;

public interface CodeDAO {

    /**
     * Get codes of all traveling certificates
     *
     * @return list of codes
     */
    List<Code> getCodes();

    /**
     * Get a code base on its value
     *
     * @param codeValue value of code
     * @return {@link Code} a code full information
     */
    Code getCode(String codeValue);
}
