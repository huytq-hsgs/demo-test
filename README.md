#Cách để viết test

#giả sử package code: src/java/main/coding

#tạo package test: src/java/main/testing

#nên để cấu trúc thư mục test giống code, mỗi file code tương ứng với 1 file test

#Xem trước 2 file: AFCUtils và AFCUtilsTest để biết cách test cơ bản

#Xem Card và CardTest để xem test bằng cách fakeData 

#Xem CodeController và CodeControllerTest, FakeCodeDAO để biết cách fake data cho những thành phần phụ thuộc 

#Xem StationController và StationControllerTest để biết cách sử dụng thư viện Mockito thay cho việc tạo những file fake