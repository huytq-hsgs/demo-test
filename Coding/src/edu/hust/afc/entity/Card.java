/**
 * @author Tran Quang Huy
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.entity;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.sql.ResultSet;
import java.sql.SQLException;

import static edu.hust.afc.entity.Code.PREPAID_CARD;

public class Card {

    public static final String CARD_IN_STATION = "IN";
    public static final String CARD_OUT_STATION = "OUT";
    private String barCode;
    private String id;
    private float balance;
    private String status;
    private Station enterStation;

    @Contract(pure = true)
    public Card(String id, float balance) {
        this.id = id;
        this.balance = balance;
    }

    @Contract(pure = true)
    public Card(String barCode, String id, float balance, String status, Station enterStation) {
        this.barCode = barCode;
        this.id = id;
        this.balance = balance;
        this.status = status;
        this.enterStation = enterStation;
    }

    public Card(@NotNull ResultSet resultSet) throws SQLException {
        this.barCode = resultSet.getString("bar_code");
        this.id = resultSet.getString("id");
        this.balance = resultSet.getFloat("balance");
        this.status = resultSet.getString("status");
        this.enterStation = null;
    }

    public static boolean isCardId(@NotNull String cardId) {
        return cardId.length() == 14 && PREPAID_CARD.equals(cardId.substring(0, 2));
    }

    @Override
    public String toString() {
        return barCode + ": Prepaid card: " + balance + " euros";
    }

    public String toInfo() {
        return "Prepaid Card - ID: " + id + " - Balance: " + balance + " euros";
    }

    public String getId() {
        return id;
    }

    public float getBalance() {
        return balance;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public Station getEnterStation() {
        return enterStation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setEnterStation(Station enterStation) {
        this.enterStation = enterStation;
    }

    public boolean isInStation() {
        return CARD_IN_STATION.equals(status);
    }
}
