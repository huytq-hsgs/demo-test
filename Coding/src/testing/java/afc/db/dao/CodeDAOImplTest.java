package testing.java.afc.db.dao;

import edu.hust.afc.db.DBConnection;
import edu.hust.afc.db.dao.CodeDAOImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CodeDAOImplTest {

    @InjectMocks
    private CodeDAOImpl codeDAO;

    @Mock
    private DBConnection dbConnection;

    @Test
    public void getStations_returnListOfStations() {
        Assert.assertNotNull(codeDAO.getCodes());
    }
}
