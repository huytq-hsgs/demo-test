/**
 * @author Tran Quang Huy
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.db.dao;

import java.util.List;

public interface CertificateDAO<T> {

    /**
     * Get data of {@code T} from database
     *
     * @param ids list id of data
     * @return list of {@code T}
     */
    List<T> getDataByIds(List<String> ids);

    /**
     * Get data of {@code T} in database from base on it's id
     *
     * @param id id of {@code T}
     * @return instance of {@code T}
     */
    T getDataById(String id);

    /**
     * Update data of {@code T} information
     *
     * @param data new data which need to update
     * @return {@code true} if update successfully, {@code false} if update failed
     */
    boolean updateData(T data);
}
